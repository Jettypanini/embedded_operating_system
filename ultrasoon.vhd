---------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.10.2019 13:01:34
-- Design Name: 
-- Module Name: ultrasoon - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ultrasoon is
    Port ( pul_in : in STD_LOGIC;
           pul_out : out STD_LOGIC;
           distance_time : out STD_LOGIC_VECTOR( 23 downto 0 ));
end ultrasoon;

architecture Behavioral of ultrasoon is

component vhdlnoclk is

  Port (
    clk65MHz : out std_logic
  );
  end component;

signal klok : std_logic;
signal counter : unsigned ( 23 downto 0 );
signal puls_counter : unsigned ( 6 downto 0 );
signal puls_out : std_logic;

begin

vhdl_clock: vhdlnoclk
port map( clk65MHz => klok );

pul_out <= '1';
puls_out <= '1';

frequency_divider: process (klok) 
begin
        if rising_edge(klok) then
            if puls_out = '1' then
                if puls_counter = "1000001" then
                    pul_out <= '0';
                    puls_out <= '0';
                    puls_counter <= "0000000";
                else
                    puls_counter <= puls_counter + 1;
                end if;
            elsif puls_out = '0' then
                counter <= counter + 1;
                if  pul_in = '1' then
                    distance_time <= std_logic_vector(counter);
                end if;
            end if;
        end if;
end process;

end Behavioral;
